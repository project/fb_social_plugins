<?php

namespace Drupal\fb_social_plugins\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\Core\Link;

/**
 * Provides the Facebook Share Button.
 *
 * @Block(
 *   id="facebook_share_button",
 *   admin_label = @Translation("Facebook Share Button"),
 * )
 */
class FbShareBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'share_button_config' => [],
    ];
  }

  /**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds body and description fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['share_button_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Like Button Configuration'),
      '#open' => true,
    ];

    $form['share_button_config'] = [
      '#type'         => 'details',
      '#title'        => $this->t('Like Button Configuration'),
      '#open'        => true,
    ];

    $form['share_button_config']['layout'] = [
      '#type'         => 'select',
      '#title'        => $this->t('Layout'),
      '#options'        => array(
        'box_count' => $this->t('box_count'),
        'button_count' => $this->t('button_count'),
        'button' => $this->t('button')
      ),
      '#default_value' => $config["layout"]??'',
    ];

    $form['share_button_config']['button_size'] = [
      '#type'         => 'select',
      '#title'        => $this->t('Button Size'),
      '#options'        => array(
        'small' => $this->t('small'),
        'large' => $this->t('large')
      ),
      '#default_value' => $config["button_size"]??'',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    if($form_state->getValue('share_button_config')) {
      $share_button_config = $form_state->getValue('share_button_config');
      $this->configuration['layout'] = $share_button_config['layout'];
      $this->configuration['button_size'] = $share_button_config['button_size'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $fb_get_current_path = fb_get_current_path();
    $url_parse = 'https://www.facebook.com/sharer/sharer.php?u='.$fb_get_current_path.'&amp;src=sdkpreparse';
    $url_parsed = urlencode($url_parse);
    $layout = $this->configuration['layout']??'standard';
    $width = $this->configuration['width']??'';
    $button_size = $this->configuration['button_size']??'small';

    $share_button_data = array(
      'current_url' => $fb_get_current_path,
      'layout' => $layout,
      'button_size' => $button_size,
      'href' => $url_parsed
    );

    $build = [];
    $build['share_button_data'] = [
      '#theme' => 'fbsp_share_button',
      '#share_button_data' => $share_button_data,
    ];
    $build['#attached']['library'][] = 'fb_social_plugins/fb_social_plugins';
    return $build;
  }

}