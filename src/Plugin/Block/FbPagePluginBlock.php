<?php

namespace Drupal\fb_social_plugins\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\Core\Link;

/**
 * Provides the Facebook Page Plugin.
 *
 * @Block(
 *   id="facebook_page_plugin",
 *   admin_label = @Translation("Facebook Page Plugin"),
 * )
 */
class FbPagePluginBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'page_plugin_config' => [],
    ];
  }

  /**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds body and description fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['page_plugin_config'] = [
      '#type'         => 'details',
      '#title'        => $this->t('Like Button Configuration'),
      '#open'        => true,
    ];

    $form['page_plugin_config']['page_url'] = [
      '#type'         => 'textfield',
      '#title'        => $this->t('Facebook Page URL'),
      '#default_value' => $config["page_url"]??'',
      '#placeholder' => $this->t('The URL of the Facebook Page'),
      '#description' => $this->t('eg. https://www.facebook.com/facebook'),
      '#required' => true
    ];

    $form['page_plugin_config']['width'] = [
      '#type'         => 'textfield',
      '#title'        => $this->t('Width'),
      '#default_value' => $config["width"]??'',
      '#placeholder' => $this->t('The pixel width of the embed (Min. 180 to Max. 500)')
    ];

    $form['page_plugin_config']['tabs'] = [
      '#type'         => 'textfield',
      '#title'        => $this->t('Tabs'),
      '#default_value' => $config["tabs"]??'timeline',
      '#placeholder' => $this->t('e.g., timeline, messages, events')
    ];

    $form['page_plugin_config']['height'] = [
      '#type'         => 'textfield',
      '#title'        => $this->t('Height'),
      '#default_value' => $config["height"]??'',
      '#placeholder' => $this->t('The pixel height of the embed (Min. 70)')
    ];

    $form['page_plugin_config']['small_header'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Small Header'),
      '#default_value' => $config["small_header"]
    ];

    $form['page_plugin_config']['hide_cover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Cover Photo'),
      '#default_value' => $config["hide_cover"]??'',
    ];

    $form['page_plugin_config']['adapt_width'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Adapt to plugin container width'),
      '#default_value' => $config["adapt_width"]??true
    ];

    $form['page_plugin_config']['show_faces'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Friend\'s Faces'),
      '#default_value' => $config["show_faces"]??true
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $page_plugin_config = $form_state->getValue('page_plugin_config');

    $this->configuration['page_url'] = $page_plugin_config['page_url'];
    $this->configuration['width'] = $page_plugin_config['width'];
    $this->configuration['tabs'] = $page_plugin_config['tabs'];
    $this->configuration['height'] = $page_plugin_config['height'];
    $this->configuration['small_header'] = $page_plugin_config['small_header'];
    $this->configuration['hide_cover'] = $page_plugin_config['hide_cover'];
    $this->configuration['adapt_width'] = $page_plugin_config['adapt_width'];
    $this->configuration['show_faces'] = $page_plugin_config['show_faces'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $page_url = $this->configuration['page_url']??'';
    $width = $this->configuration['width']??'';
    $tabs = $this->configuration['tabs']??'';
    $height = $this->configuration['height']??'';
    $small_header = $this->configuration['small_header']??'';
    $hide_cover = $this->configuration['hide_cover']??'';
    $adapt_width = $this->configuration['adapt_width']??'';
    $show_faces = $this->configuration['show_faces']??'';

    $page_plugin_data = array(
      'page_url' => $page_url,
      'width' => $width,
      'tabs' => $tabs,
      'height' => $height,
      'small_header' => $small_header,
      'hide_cover' => $hide_cover,
      'adapt_width' => $adapt_width,
      'show_faces' => $show_faces,
    );

    $build = [];
    $build['page_plugin'] = [
      '#theme' => 'fbsp_page_plugin',
      '#page_plugin_data' => $page_plugin_data,
    ];
    $build['#attached']['library'][] = 'fb_social_plugins/fb_social_plugins';
    return $build;
  }

}