<?php

namespace Drupal\fb_social_plugins\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\Core\Link;

/**
 * Provides the Facebook Like Button.
 *
 * @Block(
 *   id="facebook_like_button",
 *   admin_label = @Translation("Facebook Like Button"),
 * )
 */
class FbLikeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'like_button_config' => [],
    ];
  }

  /**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds body and description fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['like_button_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Like Button Configuration'),
      '#open' => true,
    ];

    $form['like_button_config']['layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout'),
      '#options' => array(
        'standard' => $this->t('standard'),
        'box_count' => $this->t('box_count'),
        'button_count' => $this->t('button_count'),
        'button' => $this->t('button')
      ),
      '#default_value' => $config["layout"]??'',
    ];

    $form['like_button_config']['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#default_value' => $config["width"]??'',
      '#placeholder' => $this->t('The pixel width of the plugin')
    ];

    $form['like_button_config']['action_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Action Type'),
      '#options' => array(
        'like' => $this->t('like'),
        'recommend' => $this->t('recommend')
      ),
      '#default_value' => $config["action_type"]??'',
    ];

    $form['like_button_config']['button_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Button Size'),
      '#options'  => array(
        'small' => $this->t('small'),
        'large' => $this->t('large')
      ),
      '#default_value' => $config["button_size"]??'',
    ];

    $form['like_button_config']['include_share_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include Share Button'),
      '#default_value' => $config["include_share_button"]??'',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    if($form_state->getValue('like_button_config')) {
      $like_button_config = $form_state->getValue('like_button_config');
      $this->configuration['layout'] = $like_button_config['layout'];
      $this->configuration['width'] = $like_button_config['width'];
      $this->configuration['button_size'] = $like_button_config['button_size'];
      $this->configuration['include_share_button'] = $like_button_config['include_share_button'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $layout = $this->configuration['layout']??'';
    $width = $this->configuration['width']??'';
    $button_size = $this->configuration['button_size']??'';
    $include_share_button = $this->configuration['include_share_button']??'';

    $fb_get_current_path = fb_get_current_path();

    $like_button_data = array(
      'current_url' => $fb_get_current_path,
      'layout' => $layout,
      'width' => $width,
      'button_size' => $button_size,
      'include_share_button' => $include_share_button,
    );

    $build = [];
    $build['testimonials'] = [
      '#theme' => 'fbsp_like_button',
      '#like_button_data' => $like_button_data,
    ];
    $build['#attached']['library'][] = 'fb_social_plugins/fb_social_plugins';
    return $build;
  }

}