<?php

namespace Drupal\fb_social_plugins\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\Core\Link;

/**
 * Provides the Facebook Comments Plugin.
 *
 * @Block(
 *   id="facebook_comments_plugin",
 *   admin_label = @Translation("Facebook Comments Plugin"),
 * )
 */
class FbCommentsPluginBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'comments_plugin_config' => [],
    ];
  }

  /**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds body and description fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['comments_plugin_config'] = [
      '#type'         => 'details',
      '#title'        => $this->t('Like Button Configuration'),
      '#open'        => true,
    ];

    $form['comments_plugin_config']['width'] = [
      '#type'         => 'textfield',
      '#title'        => $this->t('Width'),
      '#default_value' => $config["width"]??'',
      '#placeholder' => $this->t('The pixel width of the embed (Min. 180 to Max. 500)')
    ];

    $form['comments_plugin_config']['no_of_posts'] = [
      '#type'         => 'number',
      '#min' => 1,
      '#max' => 100,
      '#title'        => $this->t('Number of Posts'),
      '#default_value' => $config["no_of_posts"]??'5',
      '#placeholder' => $this->t('The number of posts to display by default')
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $comments_plugin_config = $form_state->getValue('comments_plugin_config');

    $this->configuration['width'] = $comments_plugin_config['width'];
    $this->configuration['no_of_posts'] = $comments_plugin_config['no_of_posts'];

  }

  /**
   * {@inheritdoc}
   */
  public function build() {

   $current_url = \Drupal\Core\Url::fromRoute('<current>', array(), array("absolute" => TRUE))->toString();

   $width = $this->configuration['width']??'';
   $no_of_posts = $this->configuration['tabs']??'5';

   $comments_plugin_data = array(
    'current_url' => $current_url,
    'width' => $width,
    'no_of_posts' => $no_of_posts,
  );

   $build = [];
   $build['comments_plugin'] = [
    '#theme' => 'fbsp_comments_plugin',
    '#comments_plugin_data' => $comments_plugin_data,
  ];
  $build['#attached']['library'][] = 'fb_social_plugins/fb_social_plugins';
  return $build;
}

}