<?php

namespace Drupal\fb_social_plugins\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;

/**
 * Configure Social sharing settings.
 */
class FbCommentsPluginSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Extension\ModuleHandler definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs a SocialSharingSettingsForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   */
  public function __construct(ModuleHandler $module_handler, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->moduleHandler = $module_handler;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fb_comments_plugin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fb_comments_plugin.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $fb_social_plugins_settings = $this->config('fb_comments_plugin.settings');

    $form['page_plugin_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Comments Plugin Configuration'),
      '#open' => true,
      '#description' => $this->t('The <em>URL to comment on</em> is the URL that this comments box is associated with. We can check the preview for the comments plugin from here <a href="https://developers.facebook.com/docs/plugins/comments">https://developers.facebook.com/docs/plugins/comments</a>')
    ];

    $form['page_plugin_configuration']['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#default_value' => $fb_social_plugins_settings->get("width"),
      '#placeholder' => $this->t('The pixel width of the embed (Min. 180 to Max. 500)')
    ];

    $form['page_plugin_configuration']['no_of_posts'] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => 100,
      '#title' => $this->t('Number of Posts'),
      '#default_value' => $fb_social_plugins_settings->get("no_of_posts")??'5',
      '#placeholder' => $this->t('The number of posts to display by default')
    ];

    $entities = getAllContentEntities();

    $form['fb_social_plugins_available_entities'] = [
      '#type' => 'details',
      '#title' => $this->t('Select the entities where the facebook Comments Plugin will be displayed'),
      '#open' => true,
      '#description' => $this->t('Flush all caches after saving the configurations to apply the changes. The <em>Facebook like button</em> field will be available on the <em>manage display</em> page where we can configure the field visibility for the entity view.'),
    ];

    // Allow modules to alter the entity types.
    $this->moduleHandler->alter('fb_social_plugins_entity_types', $entities);

    // Whitelist the entity IDs that let us link
    // to each bundle's Manage Display page.
    $linkableEntities = [
      'block_content', 'comment', 'commerce_product', 'commerce_store',
      'contact_message', 'media', 'node', 'paragraph', 'taxonomy_term', 'user',
    ];

    $disabled_entities = [
      'file', 'menu_link_content', 'shortcut', 'path_alias'
    ];

    foreach ($entities as $entity) {
      $entityId = $entity->id();
      if(!in_array($entityId, $disabled_entities)) {
        $entityType = $entity->getBundleEntityType();
      // Get all available bundles for the current entity.
        $bundles = $this->entityTypeBundleInfo->getBundleInfo($entityId);
        $links = [];

        foreach ($bundles as $machine_name => $bundle) {
          $label = $bundle['label'];

        // Some labels are TranslatableMarkup objects (such as the File entity).
          if ($label instanceof TranslatableMarkup) {
            $label = $label->render();
          }

        // Check if Field UI module enabled.
          if ($this->moduleHandler->moduleExists('field_ui')) {
          // Link to the bundle's Manage Display page
          // if the entity ID supports the route pattern.
            if (in_array($entityId, $linkableEntities) && $entityType) {
              $links[] = Link::createFromRoute($label, "entity.entity_view_display.{$entityId}.default", [
                $entityType => $machine_name,
              ])->toString();
            }
          }
        }

        $entity_label = $entity->getLabel();
        $manage_displays = empty($links) ? '' : '( ' . implode(' | ', $links) . ' )';
        if($manage_displays != '') {
          $manage_displays .= '<br>';
        }
        $manage_displays .= "Enable <em>Facebook Comments Plugin</em> for <em>{$entity_label}</em> and check manage display for the field visibility.";

        $form['fb_social_plugins_available_entities'][$entityId] = [
          '#type' => 'checkbox',
          '#title' => $this->t('@entity', ['@entity' => $entity->getLabel()]),
          '#default_value' => $fb_social_plugins_settings->get("entities.{$entityId}"),
          '#description' => $manage_displays,
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $entities = getAllContentEntities();

    $data = $this->config('fb_comments_plugin.settings');
    foreach ($entities as $entity) {
      $entityId = $entity->id();
      if(isset($values[$entityId])) {
        $data->set("entities.{$entityId}", $values[$entityId]);
      }
    }

    $data->set("width", $values['width']);
    $data->set("no_of_posts", $values['no_of_posts']);

    $data->save();

    parent::submitForm($form, $form_state);
  }

}
