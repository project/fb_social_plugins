<?php

namespace Drupal\fb_social_plugins\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;

/**
 * Configure Social sharing settings.
 */
class FbLikeSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Extension\ModuleHandler definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs a SocialSharingSettingsForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   */
  public function __construct(ModuleHandler $module_handler, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->moduleHandler = $module_handler;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fb_like_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fb_like.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $fb_social_plugins_settings = $this->config('fb_like.settings');

    $form['like_button_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Like Button Configuration'),
      '#open' => true,
      '#description' => $this->t('The <em>URL to Like</em> is the URL that this like button is associated with. We can check the preview for the like button from here <a href="https://developers.facebook.com/docs/plugins/like-button">https://developers.facebook.com/docs/plugins/like-button</a>')
    ];
    $form['like_button_configuration']['layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout'),
      '#options' => array(
        'standard' => $this->t('standard'),
        'box_count' => $this->t('box_count'),
        'button_count' => $this->t('button_count'),
        'button' => $this->t('button')
      ),
      '#default_value' => $fb_social_plugins_settings->get("layout"),
    ];
    $form['like_button_configuration']['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#default_value' => $fb_social_plugins_settings->get("width"),
      '#placeholder' => $this->t('The pixel width of the plugin')
    ];
    $form['like_button_configuration']['action_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Action Type'),
      '#options' => array(
        'like' => $this->t('like'),
        'recommend' => $this->t('recommend')
      ),
      '#default_value' => $fb_social_plugins_settings->get("action_type"),
    ];

    $form['like_button_configuration']['button_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Button Size'),
      '#options' => array(
        'small' => $this->t('small'),
        'large' => $this->t('large')
      ),
      '#default_value' => $fb_social_plugins_settings->get("button_size"),
    ];

    $form['like_button_configuration']['include_share_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include Share Button'),
      '#default_value' => $fb_social_plugins_settings->get("include_share_button"),
    ];

    $form['fb_social_plugins_available_entities'] = [
      '#type' => 'details',
      '#title' => $this->t('Select the entities where the facebook like button will be displayed'),
      '#open' => true,
      '#description' => $this->t('Flush all caches after saving the configurations to apply the changes. The <em>Facebook like button</em> field will be available on the <em>manage display</em> page where we can configure the field visibility for the entity view.'),
    ];

    $entities = getAllContentEntities();

    // Allow modules to alter the entity types.
    $this->moduleHandler->alter('fb_social_plugins_entity_types', $entities);

    // Whitelist the entity IDs that let us link
    // to each bundle's Manage Display page.
    $linkableEntities = [
      'block_content', 'comment', 'commerce_product', 'commerce_store',
      'contact_message', 'media', 'node', 'paragraph', 'taxonomy_term', 'user',
    ];

    $disabled_entities = [
      'file', 'menu_link_content', 'shortcut', 'path_alias'
    ];

    foreach ($entities as $entity) {
      $entityId = $entity->id();
      if(!in_array($entityId, $disabled_entities)) {
        $entityType = $entity->getBundleEntityType();
      // Get all available bundles for the current entity.
        $bundles = $this->entityTypeBundleInfo->getBundleInfo($entityId);
        $links = [];

        foreach ($bundles as $machine_name => $bundle) {
          $label = $bundle['label'];

        // Some labels are TranslatableMarkup objects (such as the File entity).
          if ($label instanceof TranslatableMarkup) {
            $label = $label->render();
          }

        // Check if Field UI module enabled.
          if ($this->moduleHandler->moduleExists('field_ui')) {
          // Link to the bundle's Manage Display page
          // if the entity ID supports the route pattern.
            if (in_array($entityId, $linkableEntities) && $entityType) {
              $links[] = Link::createFromRoute($label, "entity.entity_view_display.{$entityId}.default", [
                $entityType => $machine_name,
              ])->toString();
            }
          }
        }

        $entity_label = $entity->getLabel();

        $manage_displays = empty($links) ? '' : '( ' . implode(' | ', $links) . ' )';
        if($manage_displays != '') {
          $manage_displays .= '<br>';
        }
        $manage_displays .= "Enable <em>Facebook Like Button</em> for <em>{$entity_label}</em> and check manage display for the field visibility.";

        $form['fb_social_plugins_available_entities'][$entityId] = [
          '#type' => 'checkbox',
          '#title' => $this->t('@entity', ['@entity' => $entity_label]),
          '#default_value' => $fb_social_plugins_settings->get("entities.{$entityId}"),
          '#description' => $manage_displays,
        ];

      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $entities = getAllContentEntities();

    $data = $this->config('fb_like.settings');
    foreach ($entities as $entity) {
      $entityId = $entity->id();
      if(isset($values[$entityId])) {
        $data->set("entities.{$entityId}", $values[$entityId]);
      }
    }

    $data->set("layout", $values['layout']);
    $data->set("width", $values['width']);
    $data->set("action_type", $values['action_type']);
    $data->set("button_size", $values['button_size']);
    $data->set("include_share_button", $values['include_share_button']);

    $data->save();

    parent::submitForm($form, $form_state);
  }

}
