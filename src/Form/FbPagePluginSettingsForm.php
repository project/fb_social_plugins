<?php

namespace Drupal\fb_social_plugins\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;

/**
 * Configure Social sharing settings.
 */
class FbPagePluginSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Extension\ModuleHandler definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs a SocialSharingSettingsForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   */
  public function __construct(ModuleHandler $module_handler, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->moduleHandler = $module_handler;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fb_page_plugin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fb_page_plugin.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $fb_social_plugins_settings = $this->config('fb_page_plugin.settings');

    $form['page_plugin_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Page Plugin Configuration'),
      '#open' => true,
      '#description' => $this->t('We can check the preview for the page plugin from here <a href="https://developers.facebook.com/docs/plugins/page-plugin">https://developers.facebook.com/docs/plugins/page-plugin</a>')
    ];

    $form['page_plugin_configuration']['page_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook Page URL'),
      '#default_value' => $fb_social_plugins_settings->get("page_url"),
      '#placeholder' => $this->t('The URL of the Facebook Page'),
      '#required' => true,
      '#description' => $this->t('eg. https://www.facebook.com/facebook')
    ];

    $form['page_plugin_configuration']['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#default_value' => $fb_social_plugins_settings->get("width"),
      '#placeholder' => $this->t('The pixel width of the embed (Min. 180 to Max. 500)')
    ];

    $form['page_plugin_configuration']['tabs'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tabs'),
      '#default_value' => $fb_social_plugins_settings->get("tabs")??'timeline',
      '#placeholder' => $this->t('e.g., timeline, messages, events')
    ];
    $form['page_plugin_configuration']['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height'),
      '#default_value' => $fb_social_plugins_settings->get("height"),
      '#placeholder' => $this->t('The pixel height of the embed (Min. 70)')
    ];

    $form['page_plugin_configuration']['small_header'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Small Header'),
      '#default_value' => $fb_social_plugins_settings->get("small_header")
    ];
    $form['page_plugin_configuration']['hide_cover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Cover Photo'),
      '#default_value' => $fb_social_plugins_settings->get("hide_cover")
    ];
    $form['page_plugin_configuration']['adapt_width'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Adapt to plugin container width'),
      '#default_value' => $fb_social_plugins_settings->get("adapt_width")??true
    ];
    $form['page_plugin_configuration']['show_faces'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Friend\'s Faces'),
      '#default_value' => $fb_social_plugins_settings->get("show_faces")??true
    ];

    $entities = getAllContentEntities();

    $form['fb_social_plugins_available_entities'] = [
      '#type' => 'details',
      '#title' => $this->t('Select the entities where the facebook page Plugin will be displayed'),
      '#open' => true,
      '#description' => $this->t('Flush all caches after saving the configurations to apply the changes. The <em>Facebook page plugin</em> field will be available on the <em>manage display</em> page where we can configure the field visibility for the entity view.'),
    ];

    // Allow modules to alter the entity types.
    $this->moduleHandler->alter('fb_social_plugins_entity_types', $entities);

    // Whitelist the entity IDs that let us link
    // to each bundle's Manage Display page.
    $linkableEntities = [
      'block_content', 'comment', 'commerce_product', 'commerce_store',
      'contact_message', 'media', 'node', 'paragraph', 'taxonomy_term', 'user',
    ];

    $disabled_entities = [
      'file', 'menu_link_content', 'shortcut', 'path_alias'
    ];

    foreach ($entities as $entity) {
      $entityId = $entity->id();
      if(!in_array($entityId, $disabled_entities)) {
        $entityType = $entity->getBundleEntityType();
      // Get all available bundles for the current entity.
        $bundles = $this->entityTypeBundleInfo->getBundleInfo($entityId);
        $links = [];

        foreach ($bundles as $machine_name => $bundle) {
          $label = $bundle['label'];

        // Some labels are TranslatableMarkup objects (such as the File entity).
          if ($label instanceof TranslatableMarkup) {
            $label = $label->render();
          }

        // Check if Field UI module enabled.
          if ($this->moduleHandler->moduleExists('field_ui')) {
          // Link to the bundle's Manage Display page
          // if the entity ID supports the route pattern.
            if (in_array($entityId, $linkableEntities) && $entityType) {
              $links[] = Link::createFromRoute($label, "entity.entity_view_display.{$entityId}.default", [
                $entityType => $machine_name,
              ])->toString();
            }
          }
        }

        $entity_label = $entity->getLabel();
        $manage_displays = empty($links) ? '' : '( ' . implode(' | ', $links) . ' )';
        if($manage_displays != '') {
          $manage_displays .= '<br>';
        }
        $manage_displays .= "Enable <em>Facebook Page Plugin</em> for <em>{$entity_label}</em> and check manage display for the field visibility.";

        $form['fb_social_plugins_available_entities'][$entityId] = [
          '#type' => 'checkbox',
          '#title' => $this->t('@entity', ['@entity' => $entity->getLabel()]),
          '#default_value' => $fb_social_plugins_settings->get("entities.{$entityId}"),
          '#description' => $manage_displays,
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $entities = getAllContentEntities();

    $data = $this->config('fb_page_plugin.settings');
    foreach ($entities as $entity) {
      $entityId = $entity->id();
      if(isset($values[$entityId])) {
        $data->set("entities.{$entityId}", $values[$entityId]);
      }
    }

    $data->set("page_url", $values['page_url']);
    $data->set("width", $values['width']);
    $data->set("tabs", $values['tabs']);
    $data->set("small_header", $values['small_header']);
    $data->set("hide_cover", $values['hide_cover']);
    $data->set("adapt_width", $values['adapt_width']);
    $data->set("show_faces", $values['show_faces']);

    $data->save();

    parent::submitForm($form, $form_state);
  }

}
